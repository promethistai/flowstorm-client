pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
    
}


rootProject.name = "flowstorm-client"
arrayOf(
    "shared",
    "android",
    "desktop"
).forEach {
    val projectPath = ":${rootProject.name}-${it.replace('/', '-')}"
    include(projectPath)
    project(projectPath).projectDir = file(it)
}

