package ai.flowstorm.util

import ch.qos.logback.classic.LoggerContext

class AndroidLog: Log {
    override val entries: MutableList<Log.Entry> = mutableListOf()
    override val logger: Logger  = LoggerContext().getLogger("ROOT")
}