package ai.flowstorm.common.config

import ai.flowstorm.client.common.Platform
import androidx.preference.PreferenceManager

class AndroidSharedConfig : Config {
    override fun getOrNull(key: String): String? {
        if (Platform.context == null)
            return null
        val allPrefs = PreferenceManager.getDefaultSharedPreferences(Platform.context).all
        if (!allPrefs.containsKey(key)) {
            return null
        }
        return allPrefs[key].toString()
    }
    override fun set(key: String, value: String) {
        if (Platform.context == null)
            return
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(Platform.context)
        val editor = sharedPrefs.edit()
        editor.putString(key, value)
        editor.apply()
    }
}