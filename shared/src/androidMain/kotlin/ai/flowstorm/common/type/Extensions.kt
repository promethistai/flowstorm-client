package ai.flowstorm.common.type

import ai.flowstorm.core.type.Location
import android.location.Location as AndroidLocation

fun location(l: AndroidLocation) =
    Location(
        latitude = l.latitude,
        longitude = l.longitude,
        accuracy = l.accuracy.toDouble(),
        altitude = l.altitude,
        speed = l.speed.toDouble(),
        heading = l.bearing.toDouble()
    )