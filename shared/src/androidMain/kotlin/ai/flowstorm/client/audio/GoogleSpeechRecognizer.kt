package ai.flowstorm.client.audio

import ai.flowstorm.client.io.InputStreamDevice
import ai.flowstorm.client.io.NoSpeechDevice

class GoogleSpeechRecognizer : InputStreamDevice(NoSpeechDevice) {
    override fun read(buffer: ByteArray): Int = 0
    lateinit var inputRecorder: InputStreamDevice
    override fun start() {
        inputRecorder.start()
    }

    override fun stop() {
        inputRecorder.stop()
    }

}