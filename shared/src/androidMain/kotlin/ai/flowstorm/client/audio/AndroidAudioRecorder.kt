package ai.flowstorm.client.audio

import ai.flowstorm.client.io.InputStreamDevice
import ai.flowstorm.client.io.NoSpeechDevice
import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder

class AndroidAudioRecorder(var autoClose: Boolean = false) : InputStreamDevice(NoSpeechDevice) {

    companion object {

        private val SAMPLE_RATE_CANDIDATES = intArrayOf(16000, 11025, 22050, 44100)
        private val CHANNEL = AudioFormat.CHANNEL_IN_MONO
        private val ENCODING = AudioFormat.ENCODING_PCM_16BIT
    }

    var bufferSize: Int = 0
    var audioRecord: AudioRecord? = null

    override fun start() {
        audioRecord = createAudioRecord()
        audioRecord!!.startRecording()
        if (audioRecord?.recordingState == AudioRecord.RECORDSTATE_RECORDING)
            super.start()
    }

    override fun stop() {
        super.stop()
        // Fix for IllegalStateException
        if (audioRecord?.state == AudioRecord.RECORDSTATE_RECORDING)
            audioRecord?.stop()
        audioRecord?.release()
        audioRecord = null
    }

    override fun read(buffer: ByteArray): Int = audioRecord!!.read(buffer, 0, 8000)

    /**
     * Creates a new [AudioRecord].
     *
     * @return A newly created [AudioRecord], or null if it cannot be created (missing
     * permissions?).
     */
    private fun createAudioRecord(): AudioRecord {
        for (sampleRate in SAMPLE_RATE_CANDIDATES) {
            var sizeInBytes = AudioRecord.getMinBufferSize(sampleRate, CHANNEL, ENCODING)
            if (sizeInBytes == AudioRecord.ERROR_BAD_VALUE) {
                continue
            }
            sizeInBytes = sizeInBytes * 4
            val audioRecord = AudioRecord(
                MediaRecorder.AudioSource.MIC,
                sampleRate,
                CHANNEL,
                ENCODING, sizeInBytes
            )
            // logger.info("create audio record (sampleRate = $sampleRate, sizeInBytes = $sizeInBytes)")
            if (audioRecord.state == AudioRecord.STATE_INITIALIZED) {
                bufferSize = sizeInBytes
                return audioRecord
            } else {
                audioRecord.release()
            }
        }
        throw RuntimeException("Cannot instantiate AudioRecorder")
    }

    /*
    private fun isDetectingAudio(buffer: ByteArray, size: Int): Boolean {
        var i = 0
        while (i < size - 1) {
            // The buffer has LINEAR16 in little endian.
            var s = buffer[i + 1].toInt()
            if (s < 0) s *= -1
            s = s shl 8
            s += Math.abs(buffer[i].toInt())
            if (s > AMPLITUDE_THRESHOLD) {
                return true
            }
            i += 2
        }
        return false
    }
    */
}