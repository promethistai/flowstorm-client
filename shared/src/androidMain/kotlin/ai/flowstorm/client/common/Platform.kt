package ai.flowstorm.client.common

import android.app.Application
import android.content.pm.PackageManager
import android.provider.Settings

actual object Platform {
    var context: Application? = null
    actual val name = "android"
    private val allowedChars = ('a'..'f') + ('0'..'9')
    actual val version
        get() = "$name:${
                try {
                    context?.let {
                        val pInfo = it.packageManager?.getPackageInfo(it.packageName, 0)
                        pInfo?.versionName                
                    }
                } catch (e: PackageManager.NameNotFoundException) {
                    "unknown"
                }
            }"
    actual val deviceId: String
        get() {
            val generatedId = List(16) { allowedChars.random() }.joinToString("")
            val androidId = if (context == null) generatedId else Settings.Secure.getString(
                context?.contentResolver,
                Settings.Secure.ANDROID_ID
            )
            return "$name:$androidId"
        }
}