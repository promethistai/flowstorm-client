package ai.flowstorm.client.audio

//TODO implement
class IosAudioRecorder : AudioDevice {

    override var callback: AudioCallback? = null
    var started = false
        private set
    var closed = false
        private set
    private var close = false

    override fun close(waitFor: Boolean) {
        started = false
        close = true
    }

    override fun start() {
        started = true
    }

    override fun stop() {
        started = false
    }
}