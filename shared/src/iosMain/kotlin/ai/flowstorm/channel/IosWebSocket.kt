package ai.flowstorm.channel

import ai.flowstorm.common.Socket

class IosWebSocket<T : Any>(url: String) : Socket<T> {

    override lateinit var listener: Socket.Listener<T>
    override val isOpen = false
    override var state = Socket.State.New

    override fun open() {
        TODO("Not yet implemented")
    }

    override fun close() {
        TODO("Not yet implemented")
    }

    override fun send(obj: T) {
        TODO("Not yet implemented")
    }

    override fun send(data: ByteArray, count: Int?) {
        TODO("Not yet implemented")
    }


}