package ai.flowstorm.common

import platform.UIKit.UIDevice

actual object Platform {
    actual val name = "ios"
    actual val version = UIDevice.currentDevice.systemName() + "_" + UIDevice.currentDevice.systemVersion
    actual val deviceId: String = name + TODO("")
}