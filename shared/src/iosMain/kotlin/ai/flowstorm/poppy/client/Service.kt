package ai.flowstorm.poppy.client

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.channel.IosWebSocket
import ai.flowstorm.client.PlaybackHandler
import ai.flowstorm.client.SharedService
import ai.flowstorm.client.Status
import ai.flowstorm.client.StatusHandler
import ai.flowstorm.client.audio.IosAudioRecorder
import ai.flowstorm.common.config.IosAppConfig
import ai.flowstorm.common.monitoring.NoMonitoring
import ai.flowstorm.poppy.Defaults

object Service : SharedService(
    IosAppConfig(),
    Defaults,
    Callback(),
    object : StatusHandler {
        override fun get(handler: (Status?) -> Unit) {
            TODO("Not yet implemented")
        }
    },
    IosAudioRecorder(),
    TODO(""),
    PlaybackHandler { position, duration ->
        TODO("Not yet implemented")
    },
    NoMonitoring
) {
    override val webSocket get() = IosWebSocket<ChannelEvent>("${clientConfig.url}/socket/")
}