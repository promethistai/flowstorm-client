package ai.flowstorm.poppy.client

import ai.flowstorm.client.Client
import ai.flowstorm.client.ClientCallback
import ai.flowstorm.client.HttpRequest
import ai.flowstorm.core.model.LogEntry

class Callback : ClientCallback {

    override fun audio(client: Client, data: ByteArray, isSpeech: Boolean) {
        TODO("Not yet implemented")
    }

    override fun audioCancel() {
        TODO("Not yet implemented")
    }

    override fun command(client: Client, command: String, code: String?) {
        TODO("Not yet implemented")
    }

    override fun httpRequest(client: Client, url: String, request: HttpRequest?): ByteArray? {
        TODO("Not yet implemented")
    }

    override fun image(client: Client, url: String) {
        TODO("Not yet implemented")
    }

    override fun onClose(client: Client) {
        TODO("Not yet implemented")
    }

    override fun onError(client: Client, text: String) {
        TODO("Not yet implemented")
    }

    override fun onFailure(client: Client, t: Throwable) {
        TODO("Not yet implemented")
    }

    override fun onLog(client: Client, logs: MutableList<LogEntry>) {
        TODO("Not yet implemented")
    }

    override fun onOpen(client: Client) {
        TODO("Not yet implemented")
    }

    override fun onReady(client: Client) {
        TODO("Not yet implemented")
    }

    override fun onRecognized(client: Client, text: String) {
        TODO("Not yet implemented")
    }

    override fun onSessionId(client: Client, sessionId: String?) {
        TODO("Not yet implemented")
    }

    override fun onStateChange(client: Client, newState: Client.State) {
        TODO("Not yet implemented")
    }

    override fun onWakeWord(client: Client) {
        TODO("Not yet implemented")
    }

    override fun text(client: Client, text: String) {
        TODO("Not yet implemented")
    }

    override fun texts(client: Client, texts: List<String>) {
        TODO("Not yet implemented")
    }

    override fun video(client: Client, url: String) {
        TODO("Not yet implemented")
    }
}