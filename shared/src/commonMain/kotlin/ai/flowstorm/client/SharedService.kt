package ai.flowstorm.client

import ai.flowstorm.channel.ChannelEvent
import ai.flowstorm.client.io.StreamDevice
import ai.flowstorm.common.config.Config
import ai.flowstorm.client.common.Platform
import ai.flowstorm.common.monitoring.Monitoring
import ai.flowstorm.common.transport.FullDuplexSocketClient
import ai.flowstorm.common.transport.HttpSocketClientFactory
import ai.flowstorm.core.AudioFileType
import ai.flowstorm.core.ClientConfig
import ai.flowstorm.core.Input
import ai.flowstorm.core.type.Dynamic
import ai.flowstorm.util.Locale
import ai.flowstorm.util.Log
import kotlinx.datetime.Clock

abstract class SharedService(
    val config: Config,
    val defaults: Defaults,
    var callback: ClientCallbackV1,
    val statusHandler: StatusHandler,
    val inputAudioDevice: StreamDevice?,
    val log: Log,
    val playbackHandler: PlaybackHandler,
    val monitoring: Monitoring,
    val localConfig: LocalConfig
) {
    inner class Conversation {

        val messages = mutableListOf<Message>()

        fun addMessage(message: Message) {
            messages.add(message)
        }

        fun replaceLastMessage(message: Message) {
            messages.removeAt(messages.size - 1)
            addMessage(message)
        }

        fun clearMessages() {
            messages.clear()
        }
    }

    private val conversations = mutableMapOf<String, Conversation>()
    abstract val webSocket: FullDuplexSocketClient<ChannelEvent, ChannelEvent>
    abstract val webSocketFactory: HttpSocketClientFactory<ChannelEvent, ChannelEvent>
    var clientConfig: ClientConfig
    var client: Client
    var ui: ClientUI? = null

    init {
        val clientAttributes = Dynamic(
            "clientType" to  "${defaults.name}-${Platform.version}",
            "clientScreen" to true
        )
        if (localConfig.location != null) {
            clientAttributes["clientLocation"] = localConfig.location.toString()
        }
        clientConfig = ClientConfig(
            url = "https://" + config.get(defaults::coreHost.name, defaults.coreHost),
            key = config.get(defaults::appKey.name, defaults.appKey),
            token = localConfig.token,
            deviceId = config.get(Platform::deviceId.name, Platform.deviceId),
            introText = localConfig.introText,
            zoneId = localConfig.zoneId,
            locale = Locale(config.get(defaults::language.name, defaults.language)),
            autoStart = config.getOrNull(defaults::autoStart.name)?.toBoolean() ?: defaults.autoStart,
            attributes = clientAttributes,
            sendResponseItems = true,
            sttInterimResults = true,
        )
        client = if (config.get(defaults::clientVersion.name, "2") == "1") initV1Client() else initV2Client()
    }

    fun getConversation(key: String) = conversations.getOrPut(key) { Conversation() }


    private fun initV1Client(): ClientV1 = ClientV1(
        this.clientConfig,
        webSocket,
        callback,
        log,
        inputStreamDevice = inputAudioDevice,
        builtinAudioEnabled = false,
        ttsFileType = AudioFileType.mp3,
        pauseMode = false,
        monitoring = monitoring
    )

    private fun initV2Client(): ClientV2 = ClientV2(
        this.clientConfig,
        webSocketFactory,
        callback,
        log,
        inputStreamDevice = inputAudioDevice,
        builtinAudioEnabled = false,
        ttsFileType = AudioFileType.mp3,
        pauseMode = false,
        monitoring = monitoring
    )



    private fun withStatus(block: () -> Unit) {
        statusHandler.get { status ->
            val now = Clock.System.now()
            // initClientByStatus(status)
            if (status != null && status.downtime.durations.any { it.fromTime <= now && it.toTime >= now }) {
                close()
                callback.text(client, OutputItem.Text("", null, status.downtime.text, "Poppy"))
                status.downtime.audio?.let { url ->
                    callback.httpRequest(client, url)?.let { data ->
                        if (client.hasOutputAudio)
                            callback.audio(client, data, url, true, playbackHandler)
                    }
                }
            } else {
                block()
            }
        }
    }

    fun open(startMessage: String?) = withStatus {
        client.config.introText = "#${startMessage ?: "intro"}"
        client.open()
    }

    fun click(openAudioInput: Boolean) {
        if (client.state != Client.State.Listening)
            client.touch(listen = openAudioInput)
    }

    private fun skip(text: String) {
        if (client.state == Client.State.Responding || client.state == Client.State.Paused)
            client.touch(false, skip = Client.SkipMode.All)
        callback.onRecognizedText(client, text, true)
    }

    fun text(text: String) = withStatus {
        skip(text)
        client.text(text)
    }

    fun input(input: Input) = withStatus {
        skip(input.transcript.text)
        client.input(input)
    }

    fun close() {
        client.close()
        inputAudioDevice?.stop()
    }

    private fun initClientByStatus(status: Status?) {
        if (config.get(defaults::clientVersion.name, "2") != status?.clientVersion.toString() && client.state == Client.State.Closed) {
            config[defaults::clientVersion.name] = status?.clientVersion.toString()
            client = if (config.get(defaults::clientVersion.name, "2") == "1") initV1Client() else initV2Client()
        }
    }
}