package ai.flowstorm.client

import ai.flowstorm.core.type.Location
import ai.flowstorm.time.ZoneId

data class LocalConfig(
    val introText: String = "#intro",
    val zoneId: ZoneId,
    val location: Location? = null,
    val token: String? = null
)
