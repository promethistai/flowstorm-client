package ai.flowstorm.client.common

expect object Platform {
    val name: String
    val version: String
    val deviceId: String
}