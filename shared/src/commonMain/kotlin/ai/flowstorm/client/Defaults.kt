package ai.flowstorm.client

interface Defaults {

    val coreHost: String
    val streamHost: String
    val iceServerUri: String
    val appKey: String
    val name: String
    val language: String
    val autoStart: Boolean
    val enableAvatar: Boolean
    val clientVersion: String
    val debugMode: Boolean
}