package ai.flowstorm.client

data class Event (
    val name: String,
    val properties: List<EventParameter>
)
data class EventParameter(
    val name: String,
    val value: String,
    val type: String
)