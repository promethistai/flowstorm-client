package ai.flowstorm.client

data class Message(val text: String, val type: Type) {
    enum class Type { Bot, User, State, Error, Image }
}

