package ai.flowstorm.client

import ai.flowstorm.core.model.TtsConfig

interface ClientUI {

    fun onStateChange(newState: Client.State)

    fun onConnectionError(error: Throwable)

    fun onServerError(text: String, source: String)

    fun showMessage(message: String, isFromBot: Boolean, name: String? = null, replace: Boolean = false, turnId: String = "", nodeId: String? = null)

    fun showMessages(messages: List<OutputItem.Text>)

    fun showImage(url: String)

    fun showBackground(url: String)

    fun showToast(resId: Int)

    fun changeViewVisibility(viewId: Int, show: Int)

    fun loadTiles(actions: String)

    fun onSessionEnded()

    fun onSessionStarted(sessionId: String)

    fun sendDataChannelMessage(message: String)

    fun setClientReady(value: Boolean)

    fun displayUsername(username: String)

    fun logEvent(event: Event)

    fun openURL(url: String)

    fun hideUserMessages(hide: Boolean)

    fun prepareRating()

    fun showRatingUI(show: Boolean)

    fun showRankingMessage(labels: List<String>, texts: List<String>)

    fun showList(text: String)

    fun enableASRSound(enable: Boolean)

    fun setASRSilenceLength(length: Int)

    fun hideToolbar(hide: Boolean)
}