package ai.flowstorm.client

import kotlinx.datetime.Instant

class Status(val downtime: Downtime, val clientVersion: Int) {

    data class Downtime(
        val text: String,
        val audio: String? = null,
        val durations: Array<Duration> = arrayOf()
    )

    data class Duration(val fromTime: Instant, val toTime: Instant)
}