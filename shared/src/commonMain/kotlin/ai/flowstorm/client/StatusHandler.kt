package ai.flowstorm.client

interface StatusHandler {

    fun get(handler: (Status?) -> Unit)

}