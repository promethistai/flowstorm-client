import org.jetbrains.compose.compose
import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

plugins {
    kotlin("multiplatform")
    id("org.jetbrains.compose") version "1.1.0"
    id("com.android.library")
    id("maven-publish")
}

group = "ai.flowstorm.client.shared"

val flowstormVersion = findProperty("flowstorm.version") as String
val kotlinxDateTimeVersion = findProperty("kotlinx-datetime.version") as String
val iosBuild = (findProperty("iosBuild") as String).toBoolean()

kotlin {
    android {
        publishLibraryVariants("release", "debug")
        publishLibraryVariantsGroupedByFlavor = true
    }
    jvm("desktop") {
        compilations.all {
            kotlinOptions.jvmTarget = "11"
        }
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                api(compose.runtime)
                api(compose.foundation)
                api(compose.material)
                api("ai.flowstorm.client:flowstorm-client-lib:$flowstormVersion") {

                }
                api("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDateTimeVersion")
            }
        }
//        val commonTest by getting {
//            dependencies {
//                implementation(kotlin("test"))
//            }
//        }
        val androidMain by getting {
            dependencies {
                api("androidx.appcompat:appcompat:1.2.0")
                api("androidx.core:core-ktx:1.3.1")
                implementation("androidx.preference:preference-ktx:1.1.1")
                implementation("com.google.android.material:material:1.2.1")
            }
        }
//        val androidTest by getting {
//            dependencies {
//                implementation("junit:junit:4.13")
//            }
//        }
        val desktopMain by getting {
            dependencies {
                api(compose.preview)
            }
        }
//        val desktopTest by getting
        if (iosBuild) {
            val iosMain by getting {
                dependencies {
                }
            }
//            val iosTest by getting
        }
    }
}

android {
    compileSdk = 31
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
        minSdk = 26
        targetSdk = 30
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

publishing {
    repositories {
        maven {
            url = uri(findProperty("maven.repository.uri") as String)
            credentials(HttpHeaderCredentials::class) {
                System.getenv("GITLAB_PRIVATE_TOKEN")?.let {
                    name = "Private-Token"
                    value = it
                } ?: System.getenv("GITLAB_DEPLOY_TOKEN")?.let {
                    name = "Deploy-Token"
                    value = it
                } ?: System.getenv("CI_JOB_TOKEN")?.let {
                    name = "Job-Token"
                    value = it
                }
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

if (iosBuild) {
    val packForXcode by tasks.creating(Sync::class) {
        group = "build"
        val mode = System.getenv("CONFIGURATION") ?: "DEBUG"
        val sdkName = System.getenv("SDK_NAME") ?: "iphonesimulator"
        val targetName = "ios" + if (sdkName.startsWith("iphoneos")) "Arm64" else "X64"
        val framework =
            kotlin.targets.getByName<KotlinNativeTarget>(targetName).binaries.getFramework(mode)
        inputs.property("mode", mode)
        dependsOn(framework.linkTask)
        val targetDir = File(buildDir, "xcode-frameworks")
        from({ framework.outputDirectory })
        into(targetDir)
    }

    tasks.getByName("build").dependsOn(packForXcode)
}