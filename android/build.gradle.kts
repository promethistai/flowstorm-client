plugins {
    id("org.jetbrains.compose") version "1.1.0"
    id("com.android.application")
    kotlin("android")
}

group = "ai.flowstorm.client.android"
version = "1.0.0"

repositories {
    jcenter()
}

dependencies {
    implementation(project(":flowstorm-client-shared")) {
        exclude("org.jetbrains.kotlin", "kotlin-compiler-embeddable")
        exclude("org.apache.tika", "tika-core")
        exclude("javax.activation", "activation")
        exclude("jakarta.ws.rs", "jakarta.ws.rs-api")
        exclude("jakarta.xml.bind", "jakarta.xml.bind-api")
    }
    implementation("androidx.activity:activity-compose:1.3.0")
}

android {
    compileSdk = 31
    defaultConfig {
        applicationId = group.toString()
        versionName = version.toString()
        versionCode = 1
        minSdk = 26
        targetSdk = 30
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }

    packagingOptions {
        exclude("META-INF/LICENSE.*")
        exclude("META-INF/NOTICE.*")
    }
}
