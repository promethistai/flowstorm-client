buildscript {
    repositories {
        gradlePluginPortal()
        jcenter()
        google()
        mavenCentral()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.10")
        classpath("com.android.tools.build:gradle:4.1.2")
    }
}

group = "ai.flowstorm.client"
version = "1.0.0"

allprojects {
    repositories {
        google()
        mavenCentral()
        mavenLocal()
        maven { url = uri("https://csspeechstorage.blob.core.windows.net/maven/") }
        maven { url = uri("https://gitlab.com/api/v4/groups/5074098/-/packages/maven") }
        maven { url = uri("https://jitpack.io") }
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}